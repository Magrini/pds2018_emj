<?php 
	if(isset($_GET['erro'])){
		$erro_login = 'Dados inválidos, tente novamente';
	}
?>

<!-----------------------------------------------------------------------------------------------------------------
COME
------------------------------------------------------------------------------------------------------------------>

<!DOCTYPE html>
<html>
		
	<head>
	
		<meta charset=utf-8 />
		
		<!--[if lt IE 9]>
		<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<title> ColleCol Login </title>

		<link rel="stylesheet" href="css/estilos_login.css">
		
		<header class="container">
			<h1><img class="logo" src="img/logotipo_225x225.png" alt="Logo Collecol"></h1>
		</header>	
		
		

	</head>

		
		
<body>	
	
	<div id="login_box" >
	
		<div id="login_box_inside" > 
		
			<div id="login_box_label" > Log in to ColleCol				
<?php if(isset($erro_login)):?>
			<h2><?php echo $erro_login; ?></h2>
<?php endif; ?>
				<form method="POST" action="./php/login.php">
				<div class="input_div" id="input_user" >
				<input name="usuario" type="text" value="" placeholder="Usuário" />
				</div>
				
				<div class="input_div" id="input_password" >
				<input name="senha" type="password" value="" placeholder="Senha" />
				</div>
				
				<div id="buttonss" >
					
					<button type="submit" id="buttonn"> login</button>
					<div id="remember_password" > <input type="checkbox" /> Lembrar minha senha							
					</div>
									
				</div>

				<div id="cadastro" > 
					
					<a href="cadastro_usuario.php" >					
					
						<div  id="link_cadastro">Cadastre-se</div>
					
					</a>
				</div>
				</form>
			</div>
			
		</div>				
		
	</div>
	
</body>

</html>